<?php
declare(strict_types = 1);
namespace SpoonerWeb\JustVote\DataProcessing;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Http\ServerRequest;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

class HasVotedProcessor implements DataProcessorInterface
{
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData)
    {
        $frontendUser = $this->getRequest()->getAttribute('frontend.user')->user;
        $votingConfiguration = $processedData['data'];
        $extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('just_vote');
        switch ($extensionConfiguration['security']) {
            case 'username':
                $userValue = $frontendUser['username'];
                break;
            case 'hashed':
                $userValue = hash('sha256', $frontendUser['username']);
                break;
            default:
                $userValue = $frontendUser['uid'];
        }

        $hasVoted = (bool)GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_justvote_votes')
            ->count(
                '*',
                'tx_justvote_votes',
                [
                    'user' => $userValue,
                    'configuration' => $votingConfiguration['uid'],
                ]
            );

        $processedData['hasVoted'] = $hasVoted;

        return $processedData;
    }

    private function getRequest(): ServerRequest
    {
        return $GLOBALS['TYPO3_REQUEST'];
    }
}
