<?php
declare(strict_types = 1);
namespace SpoonerWeb\JustVote\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class VotingMiddleware implements MiddlewareInterface
{

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (!(($request->getQueryParams()['justvote'] ?? false) && ($request->getParsedBody()['justvote_option'] ?? false))) {
            return $handler->handle($request);
        }

        $frontendUserUid = GeneralUtility::makeInstance(Context::class)->getPropertyFromAspect('frontend.user', 'id');
        $frontendUser = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('fe_users')
            ->select(['*'], 'fe_users', ['uid' => $frontendUserUid])
            ->fetch();
        $votingConfigurationUid = (int)$request->getQueryParams()['justvote'];
        $optionUid = (int)$request->getParsedBody()['justvote_option'];
        $extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('just_vote');
        switch ($extensionConfiguration['security']) {
            case 'username':
                $result = $this->saveVote($frontendUser['username'], $votingConfigurationUid, $optionUid);
                break;
            case 'hashed':
                $result = $this->saveVote($frontendUser['username'], $votingConfigurationUid, $optionUid, true);
                break;
            default:
                $result = $this->saveVote($frontendUserUid, $votingConfigurationUid, $optionUid);
        }

        $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
        $messageQueue = $flashMessageService->getMessageQueueByIdentifier('just_vote');
        if ($result) {
            $messageQueue->addMessage(
                new FlashMessage(
                    'Thank you for your vote!',
                    'Voting succeeded',
                    FlashMessage::OK,
                    true
                )
            );
        } else {
            $messageQueue->addMessage(
                new FlashMessage(
                    'You have already voted.',
                    'Voting didn\'t work',
                    FlashMessage::ERROR,
                    true
                )
            );
        }

//        GeneralUtility::makeInstance(CacheManager::class)->flushCachesByTag('pageId_' . )

        return $handler->handle($request);
    }

    private function saveVote($user, int $configurationUid, int $optionUid, bool $hashUser = false): bool
    {
        $hashedUser = hash('sha256', $user);
        $existingVote = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_justvote_votes')
            ->count(
                '*',
                'tx_justvote_votes',
                [
                    'user' => $hashUser ? $hashedUser : $user,
                    'configuration' => $configurationUid
                ]
            );

        if ($existingVote) {
            return false;
        }

        $configurationRecord = GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_justvote_configurations')
            ->select(['pid'], 'tx_justvote_configurations', ['uid' => $configurationUid])
            ->fetch();

        return (bool)GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_justvote_votes')
            ->insert(
                'tx_justvote_votes',
                [
                    'pid' => $configurationRecord['pid'],
                    'tstamp' => time(),
                    'crdate' => time(),
                    'user' => $hashUser ? $hashedUser : $user,
                    'configuration' => $configurationUid,
                    'option' => $optionUid
                ]
            );
    }
}
