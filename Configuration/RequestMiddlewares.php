<?php

return [
    'frontend' => [
        'spooner-web/just-vote/voting' => [
            'target' => \SpoonerWeb\JustVote\Middleware\VotingMiddleware::class,
            'after' => [
                'typo3/cms-frontend/page-resolver',
            ],
        ],
    ],
];
