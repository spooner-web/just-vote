<?php

$column = [
    'voting_configuration' => [
        'exclude' => true,
        'label' => 'LLL:EXT:just_vote/Resources/Private/Language/locallang_tca.xlf:tt_content.voting_configuration',
        'l10n_mode' => 'exclude',
        'config' => [
            'type' => 'select',
            'renderType' => 'selectSingle',
            'foreign_table' => 'tx_justvote_configurations',
            'foreign_table_where' => ' AND NOT tx_justvote_configurations.hidden AND tx_justvote_configurations.sys_language_uid IN (0,-1)',
            'items' => [
                [
                    'LLL:EXT:just_vote/Resources/Private/Language/locallang_tca.xlf:please_select',
                    0
                ],
            ],
            'default' => 0,
        ]
    ]
];

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $column);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
    'tt_content',
    'CType',
    [
        'Show Voting',
        'justvote_show',
    ]
);

$GLOBALS['TCA']['tt_content']['types']['justvote_show'] = [
    'showitem' => str_replace(
        'bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel,',
        'voting_configuration,',
        $GLOBALS['TCA']['tt_content']['types']['text']['showitem']
    )
];
