<?php

$localLang = 'LLL:EXT:just_vote/Resources/Private/Language/locallang_tca.xlf:';

$typeConfig = [
    'title',
    'description',
    '--div--;' . $localLang . 'tab.access',
    'hidden',
    'starttime',
    'endtime',
    'fe_group',
    '--div--;' . $localLang . 'tab.options',
    'options'
];


return [
    'ctrl' => [
        'title' => $localLang . 'tx_justvote_configurations',
        'label' => 'title',
        'label_alt' => 'starttime,endtime',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
            'fe_group' => 'fe_group',
        ],
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'translationSource' => 'l10n_source',
        'default_sortby' => 'ORDER BY sorting',
        'sortby' => 'sorting',
    ],
    'types' => [
        0 => [
            'showitem' => implode(',', $typeConfig),
        ],
    ],
    'columns' => [
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'default' => 1,
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ]
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ]
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ]
        ],
        'fe_group' => [
            'exclude' => true,
            'l10n_mode' => 'exclude',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.fe_group',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'size' => 7,
                'maxitems' => 20,
                'foreign_table' => 'fe_groups',
                'exclusiveKeys' => '-1,-2',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hide_at_login',
                        -1
                    ],
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.any_login',
                        -2
                    ],
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.usergroups',
                        '--div--'
                    ],
                ]
            ]
        ],
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ],
                ],
                'default' => 0,
            ]
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_justvote_configurations',
                'foreign_table_where' => 'AND tx_justvote_configurations.pid=###CURRENT_PID### AND tx_justvote_configurations.sys_language_uid IN (-1,0)',
                'default' => 0,
            ]
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
                'default' => '',
            ]
        ],
        'l10n_source' => [
            'config' => [
                'type' => 'passthrough',
                'default' => 0,
            ]
        ],
        'title' => [
            'exclude' => false,
            'label' => $localLang . 'tx_justvote_configurations.title',
            'config' => [
                'type' => 'input',
                'default' => '',
                'eval' => 'required',
            ]
        ],
        'description' => [
            'exclude' => false,
            'label' => $localLang . 'tx_justvote_configurations.description',
            'config' => [
                'type' => 'text',
                'default' => '',
                'enableRichtext' => true,
            ]
        ],
        'options' => [
            'exclude' => false,
            'label' => $localLang . 'tx_justvote_configurations.options',
            'config' => [
                'type' => 'inline',
                'foreign_table' => 'tx_justvote_options',
                'foreign_field' => 'parent_record',
                'minitems' => 1,
                'maxitems' => 10,
                'appearance' => [
                    'collapseAll' => true,
                    'expandSingle' => true,
                ]
            ]
        ]
    ],
];
