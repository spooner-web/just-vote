<?php

$localLang = 'LLL:EXT:just_vote/Resources/Private/Language/locallang_tca.xlf:';

$typeConfig = [
    'user',
    'configuration',
    'option',
];


return [
    'ctrl' => [
        'title' => $localLang . 'tx_justvote_votes',
        'label' => 'user',
        'label_alt' => 'configuration,option',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'delete' => 'deleted',
    ],
    'types' => [
        0 => [
            'showitem' => implode(',', $typeConfig),
        ],
    ],
    'columns' => [
        'user' => [
            'exclude' => false,
            'label' => $localLang . 'tx_justvote_votes.user',
            'config' => [
                'type' => 'input',
                'readOnly' => true,
            ]
        ],
        'configuration' => [
            'exclude' => false,
            'label' => $localLang . 'tx_justvote_votes.configuration',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_justvote_configurations',
                'readOnly' => true,
            ]
        ],
        'option' => [
            'exclude' => false,
            'label' => $localLang . 'tx_justvote_votes.option',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_justvote_options',
                'readOnly' => true,
            ]
        ],
    ],
];
