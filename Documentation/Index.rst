
=========================
TYPO3 extension just_vote
=========================

What does it do?
================

This extension gives you an easy and small integration of a voting system for
frontend users.
The voting is only for frontend users and every user can vote once per
configuration.

Installation
============

#. :command:`composer require spooner-web/just-vote`
#. Activate extension
#. Create a voting configuration
#. Add different options to your voting
#. Integrate the voting content element on your access restricted page

Extension configuration
=======================

In the extension configuration you can define how to store the frontend user
identification in the votes table.

.. important::
   This configuration needs to set up before any voting. If you change this
   setting all other votes won't be valid any more.

There are three options:

#. UID: The uid is stored
#. Username: The username is stored
#. Hashed: The hashed username is stored

Contribution and feedback
=========================

* Visit https://git.spooner.io/spooner-web/just-vote/-/issues to create issues.
* Visit https://git.spooner.io/spooner-web/just-vote/ to get a code overview.

Contribution is welcome, feedback as well!
