<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Just Vote - A simple voting for FE users',
    'description' => 'A simple voting for FE users',
    'category' => 'fe',
    'author' => 'Thomas Löffler',
    'author_email' => 'loeffler@spooner-web.de',
    'author_company' => 'Spooner Web',
    'state' => 'beta',
    'clearCacheOnLoad' => true,
    'version' => '1.0.3',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-11.5.99',
        ],
    ],
];
