CREATE TABLE `tx_justvote_configurations` (
	`title` VARCHAR(255) DEFAULT '' NOT NULL,
	`description` TEXT NOT NULL,
	`options` INT(11) DEFAULT 0 NOT NULL
);

CREATE TABLE `tx_justvote_options` (
	`label` VARCHAR(255) DEFAULT '' NOT NULL,
	`description` TEXT NOT NULL,
	`parent_record` INT(11) DEFAULT 0 NOT NULL
);

CREATE TABLE `tx_justvote_votes` (
	`user` VARCHAR(255) DEFAULT '' NOT NULL,
	`configuration` INT(11) DEFAULT 0 NOT NULL,
	`option` INT(11) DEFAULT 0 NOT NULL
);

CREATE TABLE `tt_content` (
	`voting_configuration` INT(11) DEFAULT 0 NOT NULL
);
